<?php
function obtener_ejemplares($aplicacion)
{
    require_once "aplicacion/librerias/bd/querys_ejemplares.php";
    
    return select_ejemplares();
}

function obtener_ejemplar($aplicacion, $id_ejemplar)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    /*
     * VALIDACIONES QUE NO REQUIEREN DE ACCESO A LA BASE DE DATOS Si el id del libro que el controlador nos está solicitando no es númerico o excede una longitud de 20 caracteres no debemos perder nuestro tiempo ejecutando una consulta en la base de datos.
     */
    if (! is_numeric($id_ejemplar) || strlen($id_ejemplar) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El ejemplar no existe.';
    }
    
    /*
     * En otras "acciones" del modelo (distintas a las "acciones" del controlador) las validaciones que no requieren acceso a la base de datos serán más.
     */
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    /* Finalmente, realizamos la "consulta" a la base de datos */
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    return select_ejemplar($id_ejemplar);
}


function guardar_datos_ejemplar($aplicacion, $ejemplar)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    if (empty($ejemplar['observaciones_ejemplar'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado observaciones';
    }
    
    /* ¡¡¡Observa el schema de la base de datos!!! */
    if (empty($ejemplar['isbn'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado isbn.';
    }
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    return insert_ejemplar($ejemplar);
}

function eliminar_ejemplar($aplicacion,$id_ejemplar){
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
     if (! is_numeric($id_ejemplar) || strlen($id_ejemplar) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El ejemplar no existe.';
    }
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    
    return delete_ejemplar($id_ejemplar);
    
}

function actualizar_datos_ejemplar($aplicacion, $ejemplar){
    
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    if (empty($ejemplar['observaciones_ejemplar'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado las observaciones.';
    }
    
    /* ¡¡¡Observa el schema de la base de datos!!! */
    if (empty($ejemplar['isbn'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el isbn.';
    }
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    return update_ejemplar($ejemplar);
}





