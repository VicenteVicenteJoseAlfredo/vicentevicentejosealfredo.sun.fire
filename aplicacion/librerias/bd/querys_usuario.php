<?php

/* Requerimos de acceso a la base de datos */
require_once "aplicacion/librerias/bd/base_datos.php";

function select_usuario($user)
{
    /* Obtenemos una conexión a la base de datos */
    $bd = obtener_conexion_base_datos();
    /*
     * Si durante la conexión se presentó algún error, lo "notificamos" al modelo que nos haya llamado.
     */
    if ($bd['error'] == true) {
        return $bd;
    }
    
    $query = " select * from  usuarios  where  usuario = $1";
    
    /*
     * Ejecutamos la consulta, sobre la conexión abierta a la base de datos
     */
    $consulta = pg_query_params($bd['conexion'], $query, array(
        $user
    ));
    
    /*
     * Antes de regresar los datos o el *posible error de consulta*, cerramos la conexión a la base de datos.
     */
    cerrar_conexion_base_datos($bd['conexion']);
    
    if ($consulta == false) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se ha podido obtener información del usuario'
            )
        );
    }
    
    /*
     * Si el número de filas (rows) contenidos en el resultado de la consulta es distinto a 1 se debe a que no fue encontrado el autor con el id indicado.
     */
    if (pg_num_rows($consulta) != 1) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No existe el usuario ' . $user
            )
        );
    }
    
    /* Finalmente, regresamos los datos */
    return array(
        'error' => false,
        'datos' => pg_fetch_all($consulta)
    );
}

