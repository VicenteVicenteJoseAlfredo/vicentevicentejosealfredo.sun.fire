<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function ver_lista($aplicacion)
{
 
    $datos=  array();
    
    $tipo_vista=@$_GET['v'];
    
    if (empty($tipo_vista) || ($tipo_vista != "panel" && $tipo_vista != "tabla" && $tipo_vista != "excel" && $tipo_vista != "pdf")) {
        $datos['mensajes_error'][] = 'El tipo de vista solicitada no es reconocido.';
        $datos['vista']['titulo'] = 'Ejemplares - Lista de Ejemplares - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
    require "aplicacion/modelos/ejemplares_model.php";
    
    $resultado = obtener_ejemplares($aplicacion);
    
     if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Ejemplares - Error';
        /*
         * Existen errores que son *fatales* y se presentan en cualquier punto de la aplicación, por lo tanto utilizo una vista general para los errores.
         */
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
     $datos['ejemplares'] = $resultado['datos'];
     $datos['vista']['titulo'] = 'Ejemplares - Lista de Ejemplares';
     
     if ($tipo_vista == "panel") {
        $datos['vista']['cuerpo'] = 'html/ejemplares/panel_ejemplares.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
     /* HTML usando una tabla */
    if ($tipo_vista == "tabla") {
        $datos['vista']['cuerpo'] = 'html/ejemplares/tabla_ejemplares.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
     if ($tipo_vista == "excel") {
        require "aplicacion/vistas/excel/ejemplares/ejemplares.php";
        return true;
    }
    
    /* PDF */
    if ($tipo_vista == "pdf") {
        require "aplicacion/vistas/pdf/ejemplares/ejemplares.php";
        return true;
    }
}

function ver_ejemplar($aplicacion)
{
    
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/ejemplares_model.php";
    
    /*
     * Obtenemos el *id del autor* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_ejemplar = @$_GET['id_ejemplar'];
    $tipo_vista=@$_GET['v'];
    /* Y le solicitamos al modelo a *ese autor* en particular */
    $resultado = obtener_ejemplar($aplicacion, $id_ejemplar);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Ejemplar - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del libro */
        $datos['ejemplares'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Ejemplar- informacion de ejemplar';
        $datos['vista']['cuerpo'] = 'html/ejemplares/panel_ejemplar.php';
        $datos['vista']['tipo_vista']=$tipo_vista;
    }
    require "aplicacion/vistas/html/base/base.php";
}

function nuevo_ejemplar($aplicacion){
   
    $datos = array();
    $datos['vista']['titulo'] = 'Ejemplar';
    $datos['vista']['cuerpo'] = 'html/ejemplares/nuevo_ejemplar.php';
    require "aplicacion/vistas/html/base/base.php";
    
}

function guardar_ejemplar(){
    
    $ejemplar=$_POST['ejemplar'];
     require "aplicacion/modelos/ejemplares_model.php";
    
    $resultado = guardar_datos_ejemplar($aplicacion, $ejemplar);
    
     if ($resultado['error'] == true) {
        $datos = $resultado;
        $datos['vista']['titulo'] = "Ejemplar -Error";
        $datos['vista']['cuerpo'] = 'html/ejemplares/nuevo_ejemplar.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    $datos['ejemplar'] = $resultado['datos'];
    $datos['vista']['titulo'] = 'Ejemplar- exito nuevo ejemplar';
    $datos['vista']['cuerpo'] = 'html/ejemplares/exito_nuevo_ejemplar.php';
    require "aplicacion/vistas/html/base/base.php";
    }
    
 function borrar_ejemplar($aplicacion){
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/ejemplares_model.php";
    
    /*
     * Obtenemos el *id del autor* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_ejemplar = @$_GET['id_ejemplar'];
    $vista=@$_GET['v'];
    
    $resultado = obtener_ejemplar($aplicacion, $id_ejemplar);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Ejemplar - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['ejemplares'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Ejemplares - Información de ejemplar';
        $datos['vista']['cuerpo'] = 'html/ejemplares/borrar_ejemplar.php';
        $datos['vista']['tipo_vista']=$vista;
    }
    require "aplicacion/vistas/html/base/base.php";
}

function confirma_borrar_ejemplar($aplicacion){
    $datos=  array();
    $id_ejemplar = @$_GET['id_ejemplar'];
    $vista=@$_GET['v'];
    
    require "aplicacion/modelos/ejemplares_model.php"; 
    
    $resultado = eliminar_ejemplar($aplicacion, $id_ejemplar);
    
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Ejemplares- Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['ejemplares'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Ejemplares';
        $datos['vista']['cuerpo'] = 'html/ejemplares/exito_borrar_ejemplar.php';
        $datos['vista']['tipo_vista']=$vista;
    }
    require "aplicacion/vistas/html/base/base.php";
}

function editar_ejemplar($aplicacion){
    $datos=array();
    
   require "aplicacion/modelos/ejemplares_model.php";
    
    $id_ejemplar = @$_GET['id_ejemplar'];
    $tipo_vista=@$_GET['v'];
    
    $resultado = obtener_ejemplar($aplicacion, $id_ejemplar);
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Ejemplar - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['ejemplares'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Autores - Información de autor';
        $datos['vista']['cuerpo'] = 'html/ejemplares/editar_ejemplar.php';
        $datos['vista']['tipo_vista']=$tipo_vista;
    }
    require "aplicacion/vistas/html/base/base.php";
}

function confirma_editar_ejemplar($aplicacion){
     $ejemplar = @$_POST['ejemplar'];
     $ejemplar['id_ejemplar']=@$_GET['id_ejemplar'];
     $tipo_vista=@$_GET['v'];
    require "aplicacion/modelos/ejemplares_model.php";
    
    $resultado = actualizar_datos_ejemplar($aplicacion, $ejemplar);
    
    if ($resultado['error'] == true) {
        $datos = $resultado;
        $datos['vista']['tipo_vista'] = $tipo_vista;
        $datos['vista']['titulo'] = "Ejemplar - Actualiza - Error";
        $datos['vista']['cuerpo'] = 'html/ejemplares/editar_ejemplar.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    $datos['vista']['tipo_vista'] = $tipo_vista;
    $datos['vista']['titulo'] = "Ejemplar-Exito";
    $datos['vista']['cuerpo'] = 'html/ejemplares/exito_editar_ejemplar.php';
    require "aplicacion/vistas/html/base/base.php";
    
}



