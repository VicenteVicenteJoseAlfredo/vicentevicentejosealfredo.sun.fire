<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function ver_lista($aplicacion)
{
   
 
    $datos=  array();
    
    $tipo_vista=@$_GET['v'];
    
    if (empty($tipo_vista) || ($tipo_vista != "panel" && $tipo_vista != "tabla" && $tipo_vista != "excel" && $tipo_vista != "pdf")) {
        $datos['mensajes_error'][] = 'El tipo de vista solicitada no es reconocido.';
        $datos['vista']['titulo'] = 'Autores - Lista de autores - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
    require "aplicacion/modelos/libros_model.php";
    
    $resultado = obtener_libros($aplicacion);
    
     if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libros - Lista Libros - Error';
        /*
         * Existen errores que son *fatales* y se presentan en cualquier punto de la aplicación, por lo tanto utilizo una vista general para los errores.
         */
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
     $datos['libros'] = $resultado['datos'];
     $datos['vista']['titulo'] = 'Libros - Lista de libros';
     
     if ($tipo_vista == "panel") {
        $datos['vista']['cuerpo'] = 'html/libros/panel_libros.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
     /* HTML usando una tabla */
    if ($tipo_vista == "tabla") {
        $datos['vista']['cuerpo'] = 'html/libros/tabla_libros.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
     if ($tipo_vista == "excel") {
        require "aplicacion/vistas/excel/libros/libros.php";
        return true;
    }
    
    /* PDF */
    if ($tipo_vista == "pdf") {
        require "aplicacion/vistas/pdf/libros/libros.php";
        return true;
    }
}


function ver_libro($aplicacion)
{
   
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/libros_model.php";
    
    /*
     * Obtenemos el *id del autor* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_libro = @$_GET['id_libro'];
    $tipo_vista=@$_GET['v'];
    /* Y le solicitamos al modelo a *ese autor* en particular */
    $resultado = obtener_libro($aplicacion, $id_libro);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libro - Ver Libro - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del libro */
        $datos['libros'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Libros- informacion de libro';
        $datos['vista']['cuerpo'] = 'html/libros/panel_libro.php';
    $datos['vista']['tipo_vista']=$tipo_vista;
            
    }
    require "aplicacion/vistas/html/base/base.php";
}

function nuevo_libro($aplicacion){
    $datos = array();
    $datos['vista']['titulo'] = 'Libros- informacion de libro';
    $datos['vista']['cuerpo'] = 'html/libros/nuevo_libro.php';
    require "aplicacion/vistas/html/base/base.php";
    
}

function guardar_libro(){
    
    $libro=$_POST['libro'];
     require "aplicacion/modelos/libros_model.php";
    
    $resultado = guardar_datos_libro($aplicacion, $libro);
    
     if ($resultado['error'] == true) {
        $datos = $resultado;
        $datos['vista']['titulo'] = "Libro -Error";
        $datos['vista']['cuerpo'] = 'html/libros/nuevo_libro.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    $datos['libro'] = $resultado['datos'];
    $datos['vista']['titulo'] = 'Libros- exito nuevo libro';
    $datos['vista']['cuerpo'] = 'html/libros/exito_nuevo_libro.php';
    require "aplicacion/vistas/html/base/base.php";
    }
    
    
 function borrar_libro($aplicacion){
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/libros_model.php";
    
    /*
     * Obtenemos el *id del autor* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_libro = @$_GET['id_libro'];
    $vista=@$_GET['v'];
    
    $resultado = obtener_libro($aplicacion, $id_libro);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libro - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['libros'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Libros - Información de libro';
        $datos['vista']['cuerpo'] = 'html/libros/borrar_libro.php';
        $datos['vista']['tipo_vista']=$vista;
    }
    require "aplicacion/vistas/html/base/base.php";
}

function confirma_borrar_libro($aplicacion){
    $datos=  array();
    $id_libro = @$_GET['id_libro'];
    $vista=@$_GET['v'];
    
    require "aplicacion/modelos/libros_model.php"; 
    
    $resultado = eliminar_libro($aplicacion, $id_libro);
    
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libro- Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['libros'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Libros';
        $datos['vista']['cuerpo'] = 'html/libros/exito_borrar_libro.php';
        $datos['vista']['tipo_vista']=$vista;
    }
    require "aplicacion/vistas/html/base/base.php";
}

function editar_libro($aplicacion){
    $datos=array();
    
   require "aplicacion/modelos/libros_model.php";
    
    $id_libro = @$_GET['id_libro'];
    $tipo_vista=@$_GET['v'];
    
    $resultado = obtener_libro($aplicacion, $id_libro);
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libro - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['libros'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Libros-info';
        $datos['vista']['cuerpo'] = 'html/libros/editar_libro.php';
        $datos['vista']['tipo_vista']=$tipo_vista;
    }
    require "aplicacion/vistas/html/base/base.php";
}

function confirma_editar_libro($aplicacion){
     $libro = @$_POST['libro'];
     $libro['id_libro']=@$_GET['id_libro'];
     $tipo_vista=@$_GET['v'];
    require "aplicacion/modelos/libros_model.php";
    
    $resultado = actualizar_datos_libro($aplicacion, $libro);
    
    if ($resultado['error'] == true) {
        $datos = $resultado;
         $datos['vista']['tipo_vista'] = $tipo_vista;
        $datos['vista']['titulo'] = "Libro- Actualiza - Error";
        $datos['vista']['cuerpo'] = 'html/libros/editar_libro.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    $datos['vista']['tipo_vista'] = $tipo_vista;
    $datos['vista']['titulo'] = "Libro-Exito";
    $datos['vista']['cuerpo'] = 'html/libros/exito_editar_libro.php';
    require "aplicacion/vistas/html/base/base.php";
    
}
