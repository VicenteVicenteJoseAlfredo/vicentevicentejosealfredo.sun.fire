<?php if($_SESSION['rol']=='admin_level_1'){?>
<div class="row">
	<div class="col-md-2">
		<a href="index.php?c=libros_controller&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="index.php?c=libros_controller&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>
<?php } ?>
<?php foreach ($datos['libros'] as $libro) { ?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Libro: <strong><?php echo $libro['titulo_libro']; ?></strong>
			</div>
			<div class="panel-body">
                            
			</div>

			<div class="panel-footer clearfix">
				<div class="pull-right">
					<a href="index.php?c=libros_controller&a=ver_libro&v=panel&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-success">Información</a> 
                                        <?php if($_SESSION['rol']=='admin_level_1'){ ?>
                                        <a href="index.php?c=libros_controller&a=editar_libro&v=panel&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-success">Editar</a> 
                                        <a href="index.php?c=libros_controller&a=borrar_libro&v=panel&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-success">Borrar</a>
                                        <?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php } ?>
