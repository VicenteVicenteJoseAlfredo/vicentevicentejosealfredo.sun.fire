<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title">
			<strong>Exito</strong>
		</h3>
	</div>
	<div class="panel-body">
		<ul>
			<li>El Libro <strong><?php echo @$datos['libro']['titulo_libro']; ?></strong>
				ha sido guardado.
			</li>
		</ul>
	</div>
	<div class="panel-footer clearfix">
		<div class="pull-right">
			<a href="index.php?c=libros_controller&a=ver_libro&id_libro=<?php echo @$datos['libro']['id_libro']; ?>"
				class="btn btn-primary">Información</a> <a
				href="index.php?c=autores&a=editar_autor&id_autor=<?php echo @$datos['libro']['id_libro']; ?>"
				class="btn btn-default">Editar</a> <a
				href="index.php?c=autores&a=borrar_autor&id_autor=<?php echo @$datos['libro']['id_libro']; ?>"
				class="btn btn-warning">Borrar</a>
		</div>
	</div>
</div>