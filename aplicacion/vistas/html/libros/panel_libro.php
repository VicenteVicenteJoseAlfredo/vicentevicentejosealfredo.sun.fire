<?php foreach ($datos['libros'] as $libro) { ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		Autor: <strong><?php echo $libro['titulo_libro']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
			<li><strong>Editorial:</strong> <?php echo $libro['editorial_libro']; ?></li>
			<li><strong>ISBN:</strong> <?php echo $libro['isbn_libro']; ?></li>
                        <li><strong>Año publicacion:</strong> <?php echo $libro['anio_publicacion_libro']; ?></li>
		</ul>
	</div>

	<div class="panel-footer clearfix">
		<div class="pull-right">
                    <?php if($_SESSION['rol']=='admin_level_1'){ ?>
			<a href="index.php?c=libros_controller&a=editar_libro&v=<?php echo $datos['vista']['tipo_vista'];?>&id_libro=<?php echo $libro['id_libro']; ?>" class="btn btn-default">Editar</a>
			<a href="index.php?c=libros_controller&a=borrar_libro&v=<?php echo $datos['vista']['tipo_vista'];?>&id_libro=<?php echo $libro['id_libro']; ?>" class="btn btn-warning">Borrar</a>
                    <?php }
                    else{?>
                        			<a href="index.php?c=libros_controller&a=ver_lista&v=<?php echo $datos['vista']['tipo_vista'];?>&id_libro=<?php echo $libro['id_libro']; ?>" class="btn btn-default">Aceptar</a>
                    <?php }?>
                </div>
</div>
<?php } ?>