<form method="post" action="index.php?c=ejemplares_controller&a=confirma_editar_ejemplar&v=<?php echo $datos['vista']['tipo_vista'];?>&id_ejemplar=<?php echo $datos['ejemplares'][0]['id_ejemplar'];?>"
	class="form-horizontal" role="form">

	<div class="form-group">
		<label for="observaciones_ejemplar" class="col-md-4">Observaciones Ejemplar: <input type="text" value="<?php echo $datos['ejemplares'][0]['observaciones_ejemplar']?>" class="form-control col-md-8" name="ejemplar[observaciones_ejemplar]" id="observaciones_ejemplar" />
		</label>
	</div>

	<div class="form-group">
		<label for="isbn" class="col-md-4">ISBN:  <input type="text" value="<?php echo $datos['ejemplares'][0]['isbn']?>"  class="form-control col-md-8" 	name="ejemplar[isbn]" id="isbn" />
		</label>
	</div>

	<div class="form-group">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary">Actualizar</button> <a href="index.php?c=ejemplares_controller&a=ver_lista&v=<?php echo $datos['vista']['tipo_vista']; ?>" class="btn btn-warning">Cancelar</a>
        </div>
	</div>

</form>

<?php if (@$datos['error'] == true) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Errores</div>
			<div class="panel-body">
			<ul>
                <?php foreach (@$datos['mensajes_error'] as $error) { ?>
                <li><?php echo $error; ?></li>
                <?php } ?>
			</ul>
			</div>
		</div>
	</div>
</div>
<?php } ?>
